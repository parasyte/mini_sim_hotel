
/* Game namespace */
var game = {
    // Run on page load.
    "onload" : function onload() {
        // Initialize the video.
        if (!me.video.init("screen", c.WIDTH, c.HEIGHT, c.DOUBLEBUF, "auto", true)) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }

        // Debug.
        if (c.DEBUG) {
            me.plugin.register(debugPanel, "debug");
        }

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
        this.loadResources();

        // Initialize melonJS and display a loading screen.
        game.LoadBlipjoyLogo(function () {
            me.state.set(me.state.LOADING, new game.LoadingScreen());
            me.state.change(me.state.LOADING);
        });
    },

    "loadResources" : function loadResources() {
        // Set all resources to be loaded.
        var resources = [];

        // Graphics.
        this.resources["img"].forEach(function forEach(value) {
            resources.push({
                "name"  : value,
                "type"  : "image",
                "src"   : "resources/img/" + value + ".png"
            })
        });

        // Maps.
        this.resources["map"].forEach(function forEach(value) {
            resources.push({
                "name"  : value,
                "type"  : "tmx",
                "src"   : "resources/map/" + value + ".json"
            })
        });

        // Sound effects.
        this.resources["sfx"].forEach(function forEach(value) {
            resources.push({
                "name"      : value,
                "type"      : "audio",
                "src"       : "resources/sfx/",
                "channel"   : 3
            })
        });

        // Background music.
        this.resources["bgm"].forEach(function forEach(value) {
            resources.push({
                "name"      : value,
                "type"      : "audio",
                "src"       : "resources/bgm/",
                "channel"   : 1,
                "stream"	: true
            })
        });

        // Load the resources.
        me.loader.preload(resources);
    },

    // Run on game resources loaded.
    "loaded" : function loaded() {
        // Set up game states.
        me.state.set(me.state.BLIPJOY, new game.BlipjoyScreen());
        me.state.set(me.state.MENU, new game.TitleScreen());
        game.playscreen = new game.PlayScreen();
        me.state.set(me.state.PLAY, game.playscreen);

        // Entities.
        me.entityPool.add("person", game.Person);

        // Start the game.
        me.state.change(c.DEBUG ? me.state.PLAY : me.state.BLIPJOY);
    },

    // Helper function to print monetary values
    "money" : function money(number) {
        number = "$" + number.round(2);
        var idx = number.indexOf(".");
        if (idx < 0) number += ".00";
        else if ((idx + 3) > number.length) number += "0"
        return number;
    },

    // Helper function to determine if a variable is an Object.
    "isObject" : function isObject(object) {
        try {
            return (!Array.isArray(object) && Object.keys(object));
        }
        catch (e) {
            return false;
        }
    },

    // Simple image resize function using "nearest neighbor"
    // Only works for scaling up
    "resize" : function resize(image, scale) {
        var iw = image.width,
            ih = image.height,
            ipitch = iw * 4,
            ow = iw * scale,
            oh = ih * scale,
            opitch = ow * 4,
            context = me.video.createCanvasSurface(ow, oh);

        // Get original pixels
        context.drawImage(image, 0, 0);
        var ipixels = context.getImageData(0, 0, iw, ih);
        try {
            var opixels = context.createImageData(ow, oh);
        }
        catch(e) {
            var opixels = context.getImageData(0, 0, ow, oh);
        }

        var ix = 0,
            iy = 0;

        for (var oy = 0; oy < oh; oy++) {
            iy = Math.floor(oy / scale);
            for (var x = 0, ox = 0; x < ow; x++, ox += 4) {
                ix = Math.floor(x / scale) * 4;
                opixels.data[ox + 0 + oy * opitch] = ipixels.data[ix + 0 + iy * ipitch]; // R
                opixels.data[ox + 1 + oy * opitch] = ipixels.data[ix + 1 + iy * ipitch]; // G
                opixels.data[ox + 2 + oy * opitch] = ipixels.data[ix + 2 + iy * ipitch]; // B
                opixels.data[ox + 3 + oy * opitch] = ipixels.data[ix + 3 + iy * ipitch]; // A
            }
        }

        context.putImageData(opixels, 0, 0);

        return context.canvas;
    }

};
