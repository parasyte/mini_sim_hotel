game.resources = {
    /* Graphics. */
    "img" : [
        "background",
        "button",
        "logo",
        "hotel",
        "hotel_title",
        "rooms",
        "touch",

        /* Fake screens */
        "info_bartender",
        "info_guest",
        "info_finances",

        /* People */
        "attendant",
        "child1",
        "child2",
        "cook",
        "host",
        "hostess",
        "man1",
        "man2",
        "woman1",
        "woman2"
    ],

    /* Maps. */
    "map" : [
        "floor-1",
        "floor-2",
        "floor-3",
        "floor-4"
    ],

    /* Sound effects. */
    "sfx" : [
        "blip1",
        "blip2",
        "blip3",
        "blip4",
        "blip5",
        "blip6",
        "blip7",
        "monies"
    ],

    /* Background music. */
    "bgm" : [
        "bgmusic"
    ]
};
