game.InfoScreen = me.Renderable.extend({
    "init" : function init(image) {
        this.image = me.loader.getImage(image);

        this.parent(
            new me.Vector2d(
                ~~((c.WIDTH - this.image.width) / 2),
                ~~((c.HEIGHT - this.image.height) / 2)
            ),
            this.image.width,
            this.image.height
        );
        this.floating = true;

        me.input.registerMouseEvent("mousedown", this, (function () {
            me.game.remove(this);
            return false;
        }).bind(this));
    },

    "update" : function update() {
        return true;
    },

    "destroy" : function destroy() {
        me.input.releaseMouseEvent("mousedown", this);
    },

    "draw" : function draw(context) {
        context.globalAlpha = 0.8;
        context.drawImage(
            this.image,
            this.pos.x,
            this.pos.y,
            this.width,
            this.height
        );
        context.globalAlpha = 1;
    }
})

game.Person = me.ObjectEntity.extend({
    "init" : function init(x, y, settings) {
        this.parent(x, y, settings);

        this.updateColRect(
            ~~((this.width - this.renderable.width) / 2),
            this.renderable.width,
            ~~(this.height * 0.3),
            ~~(this.renderable.height * 0.65)
        );

        this.img = false;
        this.settings = settings;

        me.input.registerMouseEvent("mouseup", this, (function () {
            if (!game.playscreen.mousemoved) {
                // Fire "click" event on this Person
                if (this.settings.image == "hostess") {
                    console.log("Bartender");
                    me.game.add(new game.InfoScreen("info_bartender"), Infinity);
                    me.game.sort();
                }
                else if (this.settings.image == "man1") {
                    console.log("Guest");
                    me.game.add(new game.InfoScreen("info_guest"), Infinity);
                    me.game.sort();
                }
            }
        }).bind(this));
    },

    "destroy" : function destroy() {
        me.input.releaseMouseEvent("mouseup", this);
    },

    "update" : function update() {
        if (!this.walking && !~~(Math.random() * 100)) {
            this.vel.x = (Math.random() - 0.5) * 3;
            this.vel.y = (Math.random() - 0.5) * 3;

            setTimeout((function () {
                this.vel.setZero();
            }).bind(this), ~~(Math.random() * 5000));
        }

        this.updateMovement();
        this.parent();

        return true;
    }
});
