game.GUI = me.Renderable.extend({
    "init" : function init() {
        this.parent(new me.Vector2d(c.WIDTH - 128, 0), 128, c.HEIGHT);
        this.floating = true;
        this.isPersistent = true;

        this.hotel = me.loader.getImage("hotel");
        this.hotelRect = new me.Rect(
            new me.Vector2d(
                this.pos.x + 16,
                this.pos.y + 64
            ),
            this.hotel.width,
            this.hotel.height
        );

        this.moniesRect = new me.Rect(
            new me.Vector2d(
                this.pos.x,
                this.pos.y + this.height - 32
            ),
            this.width,
            32
        );

        this.font = new me.Font("bold sans serif", 24, "lime", "right");
        this.font.textBaseline = "bottom";

        // state
        this.monies = 1337;

        me.input.registerMouseEvent("mouseup", this, (function () {
            if (this.hotelRect.containsPoint(me.input.touches[0])) {
                var p = me.input.touches[0].y - this.hotelRect.pos.y;
                var floor = 4 - ~~(p / (this.hotelRect.height / 4));

                me.levelDirector.loadLevel("floor-" + floor);
            }
            else if (this.moniesRect.containsPoint(me.input.touches[0])) {
                me.game.add(new game.InfoScreen("info_finances"), Infinity);
                me.game.sort();
            }
        }).bind(this));
    },

    "onDestroyEvent" : function onDestroyEvent() {
        me.input.releaseMouseEvent("mouseup", this);
    },

    "update" : function update() {
        if (!~~(Math.random() * 50)) {
            // MAKE MONIES
            this.monies += Math.random() * 300;
            me.audio.play("monies", false, null, 0.2);
        }

        return this.visible;
    },

    "draw" : function(context) {
        // Transparent background
        var alpha = context.globalAlpha;
        context.globalAlpha = 0.6;
        context.fillStyle = "#000";
        context.fillRect(this.pos.x, this.pos.y, this.width, this.height);
        context.globalAlpha = alpha;

        // Floor-selector
        context.drawImage(
            this.hotel,
            this.hotelRect.pos.x,
            this.hotelRect.pos.y,
            this.hotelRect.width,
            this.hotelRect.height
        );

        // Monies!
        this.font.draw(
            context,
            game.money(this.monies),
            this.pos.x + this.width,
            this.pos.y + this.height
        );
    }
});

game.PlayScreen = me.ScreenObject.extend({
    "onResetEvent" : function onResetEvent() {
        // Bind controls.
        me.input.bindKey(me.input.KEY.LEFT,  "left");
        me.input.bindKey(me.input.KEY.RIGHT, "right");
        me.input.bindKey(me.input.KEY.UP,    "up");
        me.input.bindKey(me.input.KEY.DOWN,  "down");

        this.mousedown = false;
        this.mousemoved = false;
        this.mousemove = new me.Vector2d();
        this.mousedelta = new me.Vector2d();

        me.input.registerMouseEvent("mousedown", me.game.viewport, (function(e) {
            this.mousedown = true;
            this.mousemove = new me.Vector2d(
                ~~me.input.touches[0].x,
                ~~me.input.touches[0].y
            );
        }).bind(this));

        me.input.registerMouseEvent("mouseup", me.game.viewport, (function(e) {
            this.mousedown = false;
        }).bind(this));

        me.input.registerMouseEvent("mousemove", me.game.viewport, (function(e) {
            if (this.mousedown) {
                var pos = new me.Vector2d(
                    ~~me.input.touches[0].x,
                    ~~me.input.touches[0].y
                );

                this.mousedelta.copy(pos);
                this.mousedelta.sub(this.mousemove);

                this.mousemove.copy(pos);

                this.mousemoved = true;
            }
        }).bind(this));

        // GUI
        this.gui = new game.GUI();

        // "touch" display object
        var img = me.loader.getImage("touch");
        this.touch_timer = 0;
        this.touch = new me.ObjectEntity(0, 0, {
            "image" : img,
            "spritewidth" : img.width,
            "spriteheight" : img.height,
        });
        this.touch.isPersistent = true;
        this.touch.visible = false;
        this.touch.floating = true;

        me.game.add(this.gui, 1000);
        me.game.add(this.touch, Infinity);

        // Load a level.
        me.game.onLevelLoaded = this.onLevelLoaded.bind(this);
        me.levelDirector.loadLevel("floor-1");
    },

    "onLevelLoaded" : function onLevelLoaded() {
        // ...
    },

    "onDestroyEvent" : function onDestroyEvent() {
        // Unbind controls.
        me.input.unbindKey(me.input.KEY.LEFT);
        me.input.unbindKey(me.input.KEY.RIGHT);
        me.input.unbindKey(me.input.KEY.UP);
        me.input.unbindKey(me.input.KEY.DOWN);

        me.input.releaseMouseEvent("mousedown", me.game.viewport);
        me.input.releaseMouseEvent("mouseup", me.game.viewport);
        me.input.releaseMouseEvent("mousemove", me.game.viewport);

        // Destroy GUI
        me.game.remove(this.gui);
        me.game.remove(this.touch);

        // Notify listening objects of destroy
        me.event.publish("onDestroyEvent");

        this.gui = null;
        this.touch = null;
    },

    "onUpdateFrame" : function onUpdateFrame() {
        if (me.input.isKeyPressed("left")) {
            me.game.viewport.move(-9, 0);
        }
        else if (me.input.isKeyPressed("right")) {
            me.game.viewport.move(9, 0);
        }

        if (me.input.isKeyPressed("up")) {
            me.game.viewport.move(0, -9);
        }
        else if (me.input.isKeyPressed("down")) {
            me.game.viewport.move(0, 9);
        }

        if (this.mousedown) {
            // Update "touch" display
            this.touch.visible = true;
            this.touch.pos.set(
                this.mousemove.x - this.touch.hWidth,
                this.mousemove.y - this.touch.hHeight
            );

            // Remove the "touch" display after a short duration
            clearTimeout(this.touch_timer);
            this.touch_timer = setTimeout((function () {
                this.touch_timer = 0;
                this.touch.visible = false;
                this.mousemoved = false;
            }).bind(this), 50);

            if (this.gui.containsPoint(this.mousemove)) {
                // Handle GUI interactions
            }
            else if (this.mousedelta.x || this.mousedelta.y) {
                // Move viewport
                me.game.viewport.move(-this.mousedelta.x, -this.mousedelta.y);
            }

            // Reset mousedelta
            this.mousedelta.setZero();
        }

        this.parent();
    }
});
